import { StyleSheet, View, Text, TouchableOpacity} from "react-native";
import { useState } from "react";


function CountClicks() {

    const [count, setCount] = useState(0);

    return(
        <View style={styles.container}>
          <Text>{count}</Text>
          <TouchableOpacity onPress={() => setCount(count+1)}>
            <Text>Clique Aqui</Text>
          </TouchableOpacity>
        </View>
    )
  }

  export default CountClicks;

  const styles = StyleSheet.create({
    container: {
      flex: 1,
      alignItems: "center",
      justifyContent: "center",
    },
  });
  