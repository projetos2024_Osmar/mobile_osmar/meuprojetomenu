import { StyleSheet, View} from "react-native";
import ContarCaracteres from "./src/components/contarCaracteres";

function CountLetras() {
    return(
        <View style={styles.container}>
          <ContarCaracteres/>
        </View>
    )
  }

  export default CountLetras;

  const styles = StyleSheet.create({
    container: {
      flex: 1,
      alignItems: "center",
      justifyContent: "center",
    },
  });
  