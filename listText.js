import { StyleSheet, ScrollView, Text} from "react-native";

function ListText() {
    return(
        <ScrollView>
                <Text style={styles.text}>cachorro</Text>
                <Text style={styles.text}>gato</Text>
                <Text style={styles.text}>casa</Text>
                <Text style={styles.text}>arvore</Text>
                <Text style={styles.text}>computador</Text>
                <Text style={styles.text}>caneta</Text>
                <Text style={styles.text}>livro</Text>
                <Text style={styles.text}>mesa</Text>
                <Text style={styles.text}>cadeira</Text>
                <Text style={styles.text}>carro</Text>
                <Text style={styles.text}>telefone</Text>
                <Text style={styles.text}>janela</Text>
                <Text style={styles.text}>porta</Text>
                <Text style={styles.text}>lápis</Text>
                <Text style={styles.text}>copo</Text>
                <Text style={styles.text}>prato</Text>
                <Text style={styles.text}>água</Text>
                <Text style={styles.text}>fogo</Text>
                <Text style={styles.text}>terra</Text>
                <Text style={styles.text}>lua</Text>
                <Text style={styles.text}>sol</Text>
                <Text style={styles.text}>estrela</Text>
                <Text style={styles.text}>mar</Text>
                <Text style={styles.text}>rio</Text>
                <Text style={styles.text}>oceano</Text>
                <Text style={styles.text}>montanha</Text>
                <Text style={styles.text}>vale</Text>
                <Text style={styles.text}>céu</Text>
                <Text style={styles.text}>nuvem</Text>
                <Text style={styles.text}>chuva</Text>
                <Text style={styles.text}>vento</Text>
                <Text style={styles.text}>neve</Text>
                <Text style={styles.text}>gelo</Text>
                <Text style={styles.text}>frio</Text>
                <Text style={styles.text}>calor</Text>
                <Text style={styles.text}>verão</Text>
                <Text style={styles.text}>inverno</Text>
                <Text style={styles.text}>primavera</Text>
                <Text style={styles.text}>outono</Text>
                <Text style={styles.text}>floresta</Text>
                <Text style={styles.text}>selva</Text>
                <Text style={styles.text}>deserto</Text>
                <Text style={styles.text}>oceano</Text>
                <Text style={styles.text}>praia</Text>
                <Text style={styles.text}>rio</Text>
                <Text style={styles.text}>cachoeira</Text>
                <Text style={styles.text}>pássaro</Text>
                <Text style={styles.text}>borboleta</Text>
                <Text style={styles.text}>abelha</Text>
                <Text style={styles.text}>formiga</Text>
        </ScrollView>
    )
  }

  export default ListText;

  const styles = StyleSheet.create({
    container: {
      flex: 1,
      alignItems: "center",
      justifyContent: "center",
      flexDirection: 'column',
      backgroundColor:'black'
    },
    text:{
        marginTop: 8,
        fontSize: 20,
    }
  });
  