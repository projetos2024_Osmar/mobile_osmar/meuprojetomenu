import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import Menu from "./src/components/menu";
import CountLetras from "./countLetras";
import CountClicks from "./countClicks";
import ListText from "./listText";
import TextIdiomas from "./textIdiomas";
import ButtonSemFunc from "./buttonSemFunc";


const Stack = createStackNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="Menu" component={Menu}/>
        <Stack.Screen name="CountLetras" component={CountLetras}/>
        <Stack.Screen name="CountClicks" component={CountClicks}/>
        <Stack.Screen name="List50Text" component={ListText}/>
        <Stack.Screen name="TextIdiomas" component={TextIdiomas}/>
        <Stack.Screen name="ButtonSemFunc" component={ButtonSemFunc}/>
      </Stack.Navigator>
    </NavigationContainer>
  );
}
