//import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View, TouchableOpacity } from "react-native";
  
  export default function Menu({navigation}) {
    return (
      <View style={styles.container}>
        <TouchableOpacity style={styles.menu} onPress={() => navigation.navigate('CountLetras')}>
          <Text>Contagem de Caracteres</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.menu} onPress={() => navigation.navigate('CountClicks')}>
          <Text>Contagem de Cliques</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.menu} onPress={() => navigation.navigate('List50Text')}>
          <Text>Lista de Textos</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.menu} onPress={() => navigation.navigate('TextIdiomas')}>
          <Text>Saudações em Diferentes Idiomas</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.menu} onPress={() => navigation.navigate('ButtonSemFunc')}>
          <Text>Inatividade</Text>
        </TouchableOpacity>
      </View>
    );
  }
  
  const styles = StyleSheet.create({
    container: {
      flex: 1,
      alignItems: "center",
      justifyContent: "center",
    },
    menu: {
      padding: 10,
      margin: 5,
      backgroundColor: "lightblue",
      borderRadius: 5,
    },
  });
  