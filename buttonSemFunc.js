import { StyleSheet, View, TouchableOpacity, Text} from "react-native";

function ButtonSemFunc() {
    return(
        <View style={styles.container}>
          <TouchableOpacity style={styles.touch}>
            <Text>Botão Inútil</Text>
          </TouchableOpacity>
        </View>
    )
  }

  export default ButtonSemFunc;

  const styles = StyleSheet.create({
    container: {
      flex: 1,
      alignItems: "center",
      justifyContent: "center",
    },
    touch: {
        backgroundColor: 'blue',
    }
  });
  