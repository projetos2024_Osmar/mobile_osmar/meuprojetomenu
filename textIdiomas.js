import { StyleSheet, View, Text} from "react-native";

function TextIdiomas() {
    return(
        <View style={styles.container}>
          <Text style={styles.text}>Olá mundo</Text>
          <Text style={styles.text}>Hello World</Text>
          <Text style={styles.text}>Hallo Welt</Text>
          <Text style={styles.text}>Bonjour le monde</Text>
        </View>
    )
  }

  export default TextIdiomas;

  const styles = StyleSheet.create({
    container: {
      flex: 1,
      alignItems: "center",
      justifyContent: "center",
    },
    text: {
        fontSize: 28,
        marginTop: 8,
    }
  });
  